import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 *
 */
public class Controller_StartButtonPanel extends JPanel implements ActionListener {
	private JButton startButton;
	private Controller_SliderPanel sliderController;
	private View view;
	
	/**
	 * Constructs a Start Button panel. This button will gather the settings from the control panel 
	 * and instantiate a new simulation. 
	 * @param sliderController the controller that will provide the users simulation settings
	 * @param view the view that will be updated by the checkout simulation
	 */
	public Controller_StartButtonPanel(Controller_SliderPanel sliderController, View view) {
		this.sliderController = sliderController;
		this.view = view;
		createPanel();
	}
	
	/**
	 * Creates a simple panel flow layout with a button and adds an action listenter
	 */
	public void createPanel() {
		setLayout(new FlowLayout());
		
	    setBorder(BorderFactory.createLineBorder(Color.BLACK)); 
		
		startButton = new JButton("Start!");
		startButton.addActionListener(this);
		
		add(startButton);
	}
	
	/**
	 * On start button click, this method gathers the setting from the control panel, checks the at the 
	 * total number of checkouts requested by the user does not exceed 8, checks that all settings have been 
	 * gathered successfully, and finally instantiates a new Producer which runs the simulation. If more than 8 
	 * checkouts have been requested by the user, or zero checkout outs have been requested, a JOptionPane will 
	 * appear with a notification and the simulation will not be run.
	 */
	public void actionPerformed(ActionEvent e) {
		int[] settings = sliderController.getSettings();
		
		if((settings[3] + settings[4]) <= 8 && settings[3] + settings[4] > 0) {
			view.openCheckoutsAtStart(settings[3], settings[4]); //Ensures that all checkouts appear correctly at the beginning of the simulation
			if(settings.length > 0) {			
				Model_Producer producer = new Model_Producer(settings[0], settings[1], settings[2],
						settings[3], settings[4], settings[5], view);
			} else {
				System.out.println("Error with settings");
			}
		} else if(settings[3] == 0 && settings[4] == 0) {
			String errorMessage = "You must have at least one checkout for the simulation!";
			JOptionPane.showMessageDialog(null, errorMessage, "No Checkouts", 0);
		} else {
			String errorMessage = "You may only have a total of 8 normal & express checkouts!";
			JOptionPane.showMessageDialog(null, errorMessage, "Too Many Checkouts", 0);
		}
	}
}