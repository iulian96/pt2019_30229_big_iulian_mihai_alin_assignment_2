import java.util.Observable;
import java.util.Observer;

/
public class Consumer extends Thread implements Observer {
	private final int maxTimeToProcessAnItem;
	private final int maxItemsToProcess;
	private Model_Queue queue;
	private Model_Clock observableClock; 
	private View view;
	private long totalTimeToProcessCustomer;
	private Model_Customer customerTaken = null;
	private boolean running = true;
	private int numberOfCustomersProcessed = 0;
	private long checkoutOpenTime = 0;
	private long checkoutCloseTime = 0;
	private long totalTimeInUse = 0;
	
	/**
	 * Constructs the Consumer class which will pop customers from the queue and process them
	 * @param queue the queue of customers that the consumer will pop from
	 * @param maxTimeToProcessAnItem the maximum time that the consumer takes to process one item
	 * @param observableClock the clock that this consumer will observer for simulation completion
	 * @param maxItemsToProcess the maximum number of items that a checkout can process per customer
	 * @param view the view that will be updated by the consumer
	 */
	public Consumer(Model_Queue queue, int maxTimeToProcessAnItem, 
			Model_Clock observableClock, int maxItemsToProcess, View view) {
		this.view = view;
		this.queue = queue;
		this.observableClock = observableClock;
		this.maxTimeToProcessAnItem = maxTimeToProcessAnItem;
		this.maxItemsToProcess = maxItemsToProcess;
		this.observableClock.addObserver(this);
	}

	/**
	 * Sets the customers details before the checkout begins to process the customer, including (1) time checkout begins processing customer
	 * (2) the time that it will take the checkout to process the customer (3) the checkout id processing the customer
	 * (4) the state of the customer is no longer waiting the in queue
	 * @param totalTimeToProcessCustomer the number of millis that the checkout is going to take to process the customer
	 */
	public void customerPreSleepOperations(long totalTimeToProcessCustomer) {
		customerTaken.setCheckoutStartTime();
		customerTaken.setTimeToProcess(totalTimeToProcessCustomer);
		customerTaken.setCheckedOut(); 
		customerTaken.setWaitingInQueue();
	}

	/**
	 * Sets the time that the checkout finishes processing the customer, and then moves the customer to the completed list
	 */
	public void customerPostSleepOperations() {
		customerTaken.setCheckoutEndTime();
		customerTaken.addToCompleted();
	}

	/**
	 * Closes the checkout, if the consumer is in a wait state, it is interrupted, the state of the consumer is change by 
	 * setting running to false and the final update is called on the view.
	 */
	public void stopRunning() {
		checkoutCloseTime = System.currentTimeMillis();
		interrupt();
		running = false;
		view.finalUpdate();
	}

	/**
	 * Part of the observer design pattern. The consumer thread will finish once the clock notifies consumer.
	 */
	public void update(Observable o, Object arg) {
		if(observableClock == o) {
			stopRunning();
		}
	}
	
	/**
	 * Gets the maximum number of items the checkout can process (Normal or Express checkout).
	 * Normal is up to 1000 items (which should never be reached).
	 * Express is up to 10 items
	 * @return returns the maximum number of items that the checkout can process
	 */
	public int getMaxItemsToProcess() {
		return this.maxItemsToProcess;
	}
	
	/**
	 * Get the total time that the checkout has been open for
	 * @return the total time in millis that the checkout has been open for
	 */
	public long getTotalTimeInUse() {
		return totalTimeInUse;
	}
	
	/**
	 * Get the open time of the checkout
	 * @return the time in millis that the checkout has been open for
	 */
	public long getCheckoutOpenTime() {
		return checkoutOpenTime;
	}
	
	/**
	 * Get the close time of the checkout
	 * @return the time in millis that the checkout has been closed at
	 */
	public long getCheckoutCloseTime() {
		return checkoutCloseTime;
	}
	
	/**
	 * Get the count of the number of customers that the checkout has successfully processed
	 * @return the number of customers that have been processed by the checkout
	 */
	public int getTotalCustomersProcessed() {
		return numberOfCustomersProcessed;
	}
	
	/**
	 * Calculates the amount of time in millis that is required for the checkout to process the customer based on the 
	 * number of items the customer has in their trolley and the random time it takes the cashier to process an item.
	 * @param numberOfItems the number of items that the customer has in their trolley
	 * @return the total amount of time in millis that is required for the checkout to process the customer
	 */
	public long calculateCustomerProcessingTime(int numberOfItems) {
		long totalTimeToProcessCustomer = 0;
		
		while(numberOfItems > 0) {
			long timeToProcessItem = (long) 
					((Math.random() * maxTimeToProcessAnItem) + 20);
			totalTimeToProcessCustomer += timeToProcessItem;
			numberOfItems--;
		}
		return totalTimeToProcessCustomer;
	}
	
	/**
	 * Begins running consumer within its own thread. While the consumer is in a running state, 
	 * the consumer will attempt to get a customer from the queue and will process them by 
	 * setting the start time, time to process, end time, and sleeping to simulate the actual processing.
	 * After a customer has been successfully processed, the count of customers processed is increased and 
	 * the consumer thread will attempt to get another customer from the queue.
	 */
	public void run() {
		long startTime;
		long endTime;
		checkoutOpenTime = System.currentTimeMillis();

		while(running) {
			if(running) {
								
				try { customerTaken = (Model_Customer) queue.take(); } 
				catch(InterruptedException e) {	return;	}
				totalTimeToProcessCustomer = 
						calculateCustomerProcessingTime(customerTaken.getNumberOfItems());

				view.update();
				
				startTime = System.currentTimeMillis();

				customerPreSleepOperations(totalTimeToProcessCustomer);												
				
				try { sleep(totalTimeToProcessCustomer); } 
				catch(InterruptedException e) { return; }

				customerPostSleepOperations();
								
				endTime = System.currentTimeMillis();
				
				customerTaken = null;
				totalTimeInUse += (endTime - startTime);
				numberOfCustomersProcessed++;
			}
		}
}