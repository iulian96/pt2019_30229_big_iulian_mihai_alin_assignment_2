import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/**
 * This class represents the checkout GUI panel. 
 * @author Liam Hayes (13161164) 
 *
 */
public class View_CheckoutJPanel extends JPanel {

	private boolean isOpen;
	private int checkoutId, totalProcessed, checkoutType;
	
	/**
	 * Constructs a new JPanel for the checkout based on the input values. 
	 * @param isOpen boolean to display if the checkout is open (true) or closed (false)
	 * @param checkoutId the id of the checkout
	 * @param totalProcessed the total number of customers that have been processed by the checkout
	 * @param checkoutType the type of checkout (normal or express)
	 */
	public View_CheckoutJPanel(boolean isOpen, int checkoutId, int totalProcessed, int checkoutType) {
		this.isOpen = isOpen;
		this.checkoutId = checkoutId;
		this.totalProcessed = totalProcessed;
		this.checkoutType = checkoutType;
		createPanel();
	}
	
	/**
	 * Creates the checkout JPanel. If the checkout is open (true) the JPanel displays the checkout id, type, and number of customers processed.
	 * If the checkout is closed, the JPanel just display's a "closed" message.
	 */
	public void createPanel() {
	    setBorder(BorderFactory.createLineBorder(Color.BLACK)); 
		
		if(isOpen) {
			JPanel panel = new JPanel();
			panel.setLayout(new GridLayout(3,0));
			
			String type = "";
			if(checkoutType == 0) 		type = "Normal";
			else if(checkoutType == 1) 	type = "Express";
			else 						type = "Closed";
			
			String[] details = {"Cahier # " + (checkoutId + 1),
							type + " CO", 
							"Processed: " + totalProcessed};
			JLabel[] labels = new JLabel[details.length];
			labels[0] = new JLabel(details[0]);
			labels[1] = new JLabel(details[1]);
			labels[2] = new JLabel(details[2]);
			
			labels[0].setFont(new Font("Arial", Font.PLAIN, 12));
			labels[1].setFont(new Font("Arial", Font.PLAIN, 12));
			labels[2].setFont(new Font("Arial", Font.PLAIN, 12));
			
			panel.add(labels[0]);			
			panel.add(labels[1]);
			panel.add(labels[2]);
			
			add(panel);
		} else {
			JPanel closedPanel = new JPanel();
			closedPanel.setLayout(new FlowLayout());
			JLabel closedLabel = new JLabel("Closed",SwingConstants.CENTER);
			closedLabel.setFont(new Font("Arial", Font.BOLD, 12));
			closedPanel.add(closedLabel);
			closedPanel.setBackground(Color.LIGHT_GRAY);
			setBackground(Color.LIGHT_GRAY);
			
			add(closedPanel);
		}
	}
}
