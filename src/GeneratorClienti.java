import java.util.ArrayList;


public class Model_CustomerManager {
	private ArrayList<Model_Customer> completedCustomerList;
	private int numberOfCustomersBalked = 0;
	private int totalCustomersCompletedSuccess = 0;
	
	/**
	 * Constructor for the completed customer class. Creates t new ArrayList for storing the customers.
	 */
	public Model_CustomerManager() {
		completedCustomerList = new ArrayList<Model_Customer>();
	}
	
	/**
	 * Add a customer to the ArrayList
	 * @param customer The customer to add to the ArrayList
	 * @return confirmation of the successful (true) or failure (false) of adding the customer to the ArrayList
	 */
	public synchronized boolean add(Model_Customer customer) {
		if(customer.getBalked()) numberOfCustomersBalked++;
		else totalCustomersCompletedSuccess++;
		return completedCustomerList.add(customer);
	}
	
	/**
	 * Get the total number of customers that have balked status of true
	 * @return the number of customers that have balked
	 */
	public int getNumberOfCustomersBalked() {
		return numberOfCustomersBalked;
	}
	
	/**
	 * Get the total number of customers that have been processed successfully by a consumer
	 * @return the number of customers that have been processed successfully by a consumer
	 */
	public int getTotalCustomersCompletedSuccess() {
		return totalCustomersCompletedSuccess;
	}
	
	/**
	 * Get the ArrayList of completed customers
	 * @return the ArrayList of the completed customers
	 */
	public ArrayList<Model_Customer> getCompletedCustomerList() {
		return new ArrayList<Model_Customer>(completedCustomerList);
	}
	
	/**
	 * Prints the details of all customers in the ArrayList
	 */
	public void printCustomers() {
		for(Model_Customer c : completedCustomerList) {
			System.out.println("[" + c.getCustomerId() + "]" + 
								"[" + c.getNumberOfItems() + "]" +
								"[" + c.getTimeToProcess() + "]" +
								"[" + c.getCheckedOut() + "]");
		}
	}
}
