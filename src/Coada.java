import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ArrayBlockingQueue;



public class Model_Queue extends ArrayBlockingQueue {
	
	/**
	 * Creates the queue with the parameter of max capacity
	 * @param capacity the max capacity of the queue
	 */
	public Model_Queue(int capacity) {
		super(capacity);
	}
	
	/**
	 * Adds a customer to the queue if the queue is not full
	 * @param customer A customer to be added to the queue
	 * @return A string containing all of the customer's Ids and Num. of Items in the queue, along with the queue size
	 */
	public synchronized boolean put(Model_Customer customer) {
		if(remainingCapacity() != 0) {
			try { super.put(customer); } 
			catch (InterruptedException e) { e.printStackTrace(); }
			return true;
		}
		return false;
	}
	
	/**
	 * Get the current size of the queue.
	 * Locking is not needed on this method as it only reads from the queue.
	 * @return the number of customers in the queue
	 */
	public int getSizeOfQueue() {
		return super.size();
	}
	
	/**
	 * Get a sum of the total items of all customers within the queue
	 * Locking is not needed on this method as it only reads from the queue.
	 * @return a count of all of the items in the queue
	 */
	public int getTotalCustomerItems() {
		int total = 0;
		
		Iterator iterator = this.iterator();
		while(iterator.hasNext()) {
			Model_Customer c = (Model_Customer) iterator.next();
			total += c.getNumberOfItems();
		}
		
		return total;
	}
	
	
	/**
	 * Get an array of all of the customer's details currently in the queue.
	 * Locking is not needed on this method as it only reads from the queue.
	 * @return An array containing all of the customer's ids and number of items
	 */
	public String[] getCustomerDeailsFromQueue() {			
		ArrayList<String> detailsList = new ArrayList<String>();
		String[] detailsArray = new String[6];
		
		Iterator iterator = this.iterator();
		while(iterator.hasNext()) {
			Model_Customer c = (Model_Customer) iterator.next();
			detailsList.add(c.getCustomerId() + ":" + c.getNumberOfItems());
		}
		
		for(int i = 0; i < detailsArray.length; i++) {
			if(i < detailsList.size()) {
				if(!detailsList.get(i).equals("")) {
					detailsArray[i] = detailsList.get(i);
				}
			} else {
				detailsArray[i] = "";
			}
		}
		
		return detailsArray;
	}
}