import java.awt.BorderLayout;
import java.awt.Container;

import javax.swing.JFrame;
import javax.swing.JPanel;



public class View_SimulationFrame extends JFrame {
	
	/**
	 * Constructs a new simulation frame with all of the components and sets it visible to the user.
	 */
	public View_SimulationFrame() {
		super("Checkout Simulation");
		Container contentPane = getContentPane();
		setLayout(new BorderLayout());
		
		JPanel controls = new JPanel();
		JPanel checkoutInterface = new JPanel();
		
		controls.setLayout(new BorderLayout());
		checkoutInterface.setLayout(new BorderLayout());
		
		View view = new View();
		Controller_SliderPanel sliderController = new Controller_SliderPanel();
		Controller_StartButtonPanel startButtonController = new Controller_StartButtonPanel(sliderController, view);
		
		controls.add(sliderController, BorderLayout.CENTER);
		controls.add(startButtonController, BorderLayout.SOUTH);
		checkoutInterface.add(view);
		
		contentPane.add(controls, BorderLayout.WEST);
		contentPane.add(checkoutInterface, BorderLayout.CENTER);
		
		pack(); //pack the frame automatically. Could also use setSize(1200, 800) for better sizing
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}
}